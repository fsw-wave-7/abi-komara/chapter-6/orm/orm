const express = require('express')
const morgan = require('morgan')
const path = require('path')
const { join } = require('path')
const { allowedNodeEnvironmentFlags } = require('process')
const api = require('./routes/api')
const web = require('./routes/web')

const app = express()
const port = 3000

app.use(express.static(__dirname + '/public'))

app.set('view engine', 'ejs')

app.use(morgan('dev'))

app.use('/api', api)
app.use('/', web)

app.get('/login', function(req, res) {
    res.render(path.join(__dirname, '/views/login'))
})

app.get('/signup', function(req, res) {
    res.render(path.join(__dirname, '/views/form'))
})

app.get('/list', function(req, res) {
    res.render(path.join(__dirname, '/views/table'))
})

app.use((err, req, res, next) => {
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})

app.use((req, res, next) => {
    res.render(join(__dirname, './views/404'))
})

app.listen(port, () => { console.log('Server Running') })